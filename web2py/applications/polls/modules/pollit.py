from bs4 import BeautifulSoup
import requests

#url='https://en.wikipedia.org/wiki/Nationwide_opinion_polling_for_the_Democratic_Party_2016_presidential_primaries'
url='https://en.wikipedia.org/wiki/Nationwide_opinion_polling_for_the_Republican_Party_2016_presidential_primaries'
result = requests.get(url)
strContestants = []
soup=BeautifulSoup(result.content,"html.parser")
tables =  soup.find_all("table")

#Table-1 is side table
#Table-2 is aggregate polling
rows = tables[1].find_all("tr")

#Get the list of contestants first
columns= rows[0].find_all("td")
for column in columns:
#    print column.find("a").get('title')
    strContestants.append(column.find("a").get('title'))

#Now push others into the array
strContestants.append("Others")

floatPollCount = []
for i in range(len(strContestants)):
    floatPollCount.append([])
numContestantsCounted = 0
#Now get the results for the aggregate polls
for index in range(2,len(strContestants)+1):
    for index2 in range(1,len(rows)):
        #print "index2 is "+str(index2)
        #print rows[index2].findAll('td')[index]
        descendants = rows[index2].findAll('td')[index].descendants
        temparr = []
        for descendant in descendants:
            temparr.append(descendant)
        #print "last descendant is"+temparr[-1]
        try:
            floatPollCount[numContestantsCounted].append(float(temparr[-1][:-1]))
        except ValueError:
            floatPollCount[numContestantsCounted].append(0.0)
    numContestantsCounted += 1
for i in strContestants:
    print i
for i in floatPollCount:
    print i
            
        
    


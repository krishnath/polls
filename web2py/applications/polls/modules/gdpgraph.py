#!/usr/local/bin/python2.7
import xlrd
import numpy as np
import matplotlib.pyplot as plt
import sys
import base64
fname = "/Users/krish/Downloads/web2py/applications/polls/static/datafiles/gdplev.xls"
xl_workbook = xlrd.open_workbook(fname)
arrGdp = xl_workbook.sheet_by_index(0).col_values(1,start_rowx=8, end_rowx=94)
arrYears = xl_workbook.sheet_by_index(0).col_values(0,start_rowx=8, end_rowx=94)
arrYears = [int(year) for year in arrYears]


def getGdp(fromyear, toyear):
	fromindex = arrYears.index(int(fromyear))
	toindex = arrYears.index(int(toyear))
	t = arrYears[fromindex:toindex+1]
	s = arrGdp[fromindex:toindex+1]
	ax = plt.gca()
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	plt.plot(t, s,marker='o')
	plt.xlabel('time (s)')
	plt.ylabel('GDP billions')
	plt.title('gdp from %s to %s' % (fromyear,toyear))
	plt.grid(True)
	outputfilename='%sto%s.png'% (fromyear,toyear)
	plt.savefig(outputfilename)
	with open(outputfilename, "rb") as imageFile:
        	str = base64.b64encode(imageFile.read())
	return str
#	plt.show()


fromyear=int(sys.argv[1])
toyear=int(sys.argv[2])
getGdp(fromyear, toyear)

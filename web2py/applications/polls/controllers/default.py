# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
from bs4 import BeautifulSoup
import requests
import subprocess
import xlrd
#import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
import sys
import base64
import time

#datafolder="/home/www-data/polls/polls/web2py/applications/polls/static/datafiles/";
datafolder="/Users/krish/Downloads/web2py/applications/polls/static/datafiles/";
def GrossDP() :
    
    
    fname=datafolder+"gdplev.xls"
    
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).col_values(0,start_rowx=8, end_rowx=94)
    arrYears = [int(year) for year in arrYears]
    return dict(message=T('View Gross DP'),min=arrYears[0],max=arrYears[len(arrYears)-1])



def gdpGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    
    fname=datafolder+"gdplev.xls"
    xl_workbook = xlrd.open_workbook(fname)
    arrGdp = xl_workbook.sheet_by_index(0).col_values(1,start_rowx=8, end_rowx=94)
    arrYears = xl_workbook.sheet_by_index(0).col_values(0,start_rowx=8, end_rowx=94)
    arrYears = [int(year) for year in arrYears]
    
    

    
    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    
    t = arrYears[fromindex:toindex+1]
    
    s = arrGdp[fromindex:toindex+1]
    
    return dict(years=t,gdp=s)

def motorprice():
    #below is production location
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    return dict(message=T('View Motor Price Index!!'),min=arrYears[0],max=arrYears[len(arrYears)-1])

    
def motorGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    arrValues = xl_workbook.sheet_by_index(0).row_values(12,start_colx=3, end_colx=49)
    arrYears = [int(year) for year in arrYears]
    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    t = arrYears[fromindex:toindex+1]
    s = arrValues[fromindex:toindex+1]
    return dict(years=t,values=s)



def foodprice():
    #below is production location
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    return dict(message=T('View Food and Beverage Price Index!!'),min=arrYears[0],max=arrYears[len(arrYears)-1])

    
def foodGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    arrValues = xl_workbook.sheet_by_index(0).row_values(17,start_colx=3, end_colx=49)
    arrYears = [int(year) for year in arrYears]
    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    t = arrYears[fromindex:toindex+1]
    s = arrValues[fromindex:toindex+1]
    return dict(years=t,values=s)


def transprice():
    #below is production location
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    return dict(message=T('View Transportation Price Index!!'),min=arrYears[0],max=arrYears[len(arrYears)-1])

    
def transGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    arrValues = xl_workbook.sheet_by_index(0).row_values(25,start_colx=3, end_colx=49)
    arrYears = [int(year) for year in arrYears]
    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    t = arrYears[fromindex:toindex+1]
    s = arrValues[fromindex:toindex+1]
    return dict(years=t,values=s)

def finprice():
    #below is production location
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    return dict(message=T('View Finacial Services Price Index!!'),min=arrYears[0],max=arrYears[len(arrYears)-1])

    
def finGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    arrValues = xl_workbook.sheet_by_index(0).row_values(28,start_colx=3, end_colx=49)
    arrYears = [int(year) for year in arrYears]
    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    t = arrYears[fromindex:toindex+1]
    s = arrValues[fromindex:toindex+1]
    return dict(years=t,values=s)
    

def politicalnews():
    return dict(message=T('View Political News!!'))

def energyprice():
    #below is production location
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    return dict(message=T('View Gasoline Price Index!!'),min=arrYears[0],max=arrYears[len(arrYears)-1])

def energyGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    arrHealthCareIndices = xl_workbook.sheet_by_index(0).row_values(19,start_colx=3, end_colx=49)
    arrYears = [int(year) for year in arrYears]
    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    t = arrYears[fromindex:toindex+1]
    s = arrHealthCareIndices[fromindex:toindex+1]
    return dict(years=t,hcp=s)
       

def healthcareprice():
    #below is production location
    

    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"


    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    return dict(message=T('View Health Care Data!!'),min=arrYears[0],max=arrYears[len(arrYears)-1])
    
    

def hcpGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']

    
    fname=datafolder+"PricingIndicesForGrossDomesticPurchases.xlsx"

    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(0).row_values(7,start_colx=3, end_colx=49)
    arrHealthCareIndices = xl_workbook.sheet_by_index(0).row_values(24,start_colx=3, end_colx=49)
    
    
    arrYears = [int(year) for year in arrYears]

    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    
    t = arrYears[fromindex:toindex+1]
    s = arrHealthCareIndices[fromindex:toindex+1]
    return dict(years=t,hcp=s)

    
    

def homeownership():
    fname=datafolder+"homeownershipcln.xls"
    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(1).col_values(0,start_rowx=2, end_rowx=53)
    arrYears = [int(year) for year in arrYears]
    return dict(message=T('View Home Ownership'),min=arrYears[0],max=arrYears[len(arrYears)-1])


def hoGraph():
    time.sleep(4)
    fromyear = request.vars['fromyear']
    toyear = request.vars['toyear']
    fname=datafolder+"homeownershipcln.xls"

    xl_workbook = xlrd.open_workbook(fname)
    arrYears = xl_workbook.sheet_by_index(1).col_values(0,start_rowx=2, end_rowx=53)
    arrOwnershipRates = xl_workbook.sheet_by_index(1).col_values(5,start_rowx=2, end_rowx=53)
    arrYears = [int(year) for year in arrYears]
    
    

    fromindex = arrYears.index(int(fromyear))
    toindex = arrYears.index(int(toyear))
    
    t = arrYears[fromindex:toindex+1]
    s = arrOwnershipRates[fromindex:toindex+1]
    return dict(years=t,ho=s)

#     ax = plt.gca()
#     ax.get_xaxis().get_major_formatter().set_useOffset(False)
#     plt.plot(t, s,marker='o')
#     plt.xlabel('year (s)')
#     plt.ylabel('Ownership Percentage')
#     plt.title('Home Ownership rates from %s to %s' % (fromyear,toyear))
#     plt.grid(True)
#     outputfilename='%sto%s.png'% (fromyear,toyear)
#     plt.savefig(outputfilename)
#     
#     with open(outputfilename, "rb") as imageFile:
#             str = base64.b64encode(imageFile.read())
#     return dict(graph=str)
    
    
    
    
def gdp():
    return dict(message=T('View GDP information!!'))
     
# def gdpGraph():
#     
#     fromyear = request.vars['fromyear']
#     toyear = request.vars['toyear']
#     #fname="/home/www-data/polls/polls/web2py/applications/polls/static/datafiles/gdplev.xls"
#     fname = "/Users/krish/Downloads/web2py/applications/polls/static/datafiles/gdplev.xls"
#     xl_workbook = xlrd.open_workbook(fname)
#     arrGdp = xl_workbook.sheet_by_index(0).col_values(1,start_rowx=8, end_rowx=94)
#     arrYears = xl_workbook.sheet_by_index(0).col_values(0,start_rowx=8, end_rowx=94)
#     arrYears = [int(year) for year in arrYears]
#     
#     
#     
#     fromindex = arrYears.index(int(fromyear))
#     
#     toindex = arrYears.index(int(toyear))
#     
#     t = arrYears[fromindex:toindex+1]
#     s = arrGdp[fromindex:toindex+1]
#     
#     ax = plt.gca()
#     ax.get_xaxis().get_major_formatter().set_useOffset(False)
#     plt.plot(t, s,marker='o')
#     plt.xlabel('time (s)')
#     plt.ylabel('GDP billions')
#     plt.title('gdp from %s to %s' % (fromyear,toyear))
#     plt.grid(True)
#     outputfilename='%sto%s.png'% (fromyear,toyear)
#     plt.savefig(outputfilename)
#     
#     with open(outputfilename, "rb") as imageFile:
#             str = base64.b64encode(imageFile.read())
#     return dict(graph=str)
    
    
    

def bargraphdemocratic():
    return dict(message=T('View latest news!!'))

def bargraph():
    
    return dict(message=T('View latest news!!'))

def demonews():
    
    return dict(message=T('View latest news!!'))

def democratictweets():
    
    return dict(message=T('View latest tweets!!'))

def republicantweets():
    
    return dict(message=T('View latest tweets!!'))

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hi!!!!!")
    return dict(message=T('Welcome to web2py!'))

def democratic():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def democraticScores():
    url='https://en.wikipedia.org/wiki/Nationwide_opinion_polling_for_the_Democratic_Party_2016_presidential_primaries'
    result = requests.get(url)
    strContestants = []
    soup=BeautifulSoup(result.content,"html.parser")
    tables =  soup.find_all("table")

    #Table-1 is side table
    #Table-2 is aggregate polling
    rows = tables[1].find_all("tr")

    #Get the list of contestants first
    columns= rows[0].find_all("td")
    for column in columns:
        #    print column.find("a").get('title')
        strContestants.append(column.find("a").get('title'))

    #Now push others into the array
    strContestants.append("Others")

    floatPollCount = []
    for i in range(len(strContestants)):
        floatPollCount.append([])
    numContestantsCounted = 0
    #Now get the results for the aggregate polls
    for index in range(2,len(strContestants)+1):
        for index2 in range(1,len(rows)):
            #print "index2 is "+str(index2)
            #print rows[index2].findAll('td')[index]
            descendants = rows[index2].findAll('td')[index].descendants
            temparr = []
            for descendant in descendants:
                temparr.append(descendant)
                #print "last descendant is"+temparr[-1]
            try:
            
                floatPollCount[numContestantsCounted].append(float(temparr[-1][:-1]))
            except ValueError:
                floatPollCount[numContestantsCounted].append(0.0)
        numContestantsCounted += 1
        for i in strContestants:
            print i
        for i in floatPollCount:
            print i
    return dict(strContestants=strContestants,floatPollCount=floatPollCount)
    
def getscores():
    url='https://en.wikipedia.org/wiki/Nationwide_opinion_polling_for_the_Republican_Party_2016_presidential_primaries'
    result = requests.get(url)
    strContestants = []
    soup=BeautifulSoup(result.content,"html.parser")
    tables =  soup.find_all("table")

    #Table-1 is side table
    #Table-2 is aggregate polling
    rows = tables[1].find_all("tr")

    #Get the list of contestants first
    columns= rows[0].find_all("td")
    for column in columns:
        #    print column.find("a").get('title')
        strContestants.append(column.find("a").get('title'))

    #Now push others into the array
    strContestants.append("Others")

    floatPollCount = []
    for i in range(len(strContestants)):
        floatPollCount.append([])
    numContestantsCounted = 0
    #Now get the results for the aggregate polls
    for index in range(2,len(strContestants)+1):
        for index2 in range(1,len(rows)):
            #print "index2 is "+str(index2)
            #print rows[index2].findAll('td')[index]
            descendants = rows[index2].findAll('td')[index].descendants
            temparr = []
            for descendant in descendants:
                temparr.append(descendant)
                #print "last descendant is"+temparr[-1]
            try:
            
                floatPollCount[numContestantsCounted].append(float(temparr[-1][:-1]))
            except ValueError:
                floatPollCount[numContestantsCounted].append(0.0)
        numContestantsCounted += 1
        for i in strContestants:
            print i
        for i in floatPollCount:
            print i
    return dict(strContestants=strContestants,floatPollCount=floatPollCount)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

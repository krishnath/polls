# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
# # Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(B('web', SPAN(2), 'py'), XML('&trade;&nbsp;'),
                  _class="navbar-brand", _href="{{=URL('default','index')}}",
                  _id="web2py-logo")
response.title = request.application.replace('_', ' ').title()
response.subtitle = ''

# # read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Your Name <you@example.com>'
response.meta.description = 'a cool new app'
response.meta.keywords = 'web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'

# # your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
# # this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('Home'), False, URL('default', 'index'), [])
]

DEVELOPMENT_MENU = True

#########################################################################
# # provide shortcuts for development. remove in production
#########################################################################

def _():
    # shortcuts
    app = request.application
    ctr = request.controller
    # useful links to internal and external resources
    response.menu += [
#         (T('My Sites'), False, URL('admin', 'default', 'site')),
          (T('Party level Polls'), False, '#', [
                                       (T('Democratic'), False, URL('default', 'democratic'), []),
                                                        
    (T('Republican'), False, URL('default', 'index'), []),
#                          (T('Republic BarGraph'), False, URL('default', 'bargraph'), []),
#                                                         (T('Democratic BarGraph'), False, URL('default', 'bargraphdemocratic'), []),
              ]),
        (T('Trending tweets'), False, '#', [
                        (T('Democratic'), False, URL('default', 'democratictweets'), []),
                        (T('Republican'), False, URL('default', 'republicantweets'), []),
        ]),
                      
(T('Trending news'), False, URL('default', 'politicalnews'), []),

(T('Analyse economy of USA in the past'), False, '#', [(T('Compare GDP of previous years'), False, URL('default', 'GrossDP'), []),
                                                     (T('Compare Home Ownership Percentage of previous years'), False, URL('default', 'homeownership'),[]),
                                                     (T('Compare Health Care pricing index previous years'), False, URL('default', 'healthcareprice'),[]),
                                                     (T('Compare Energy pricing index of  previous years'), False, URL('default', 'energyprice'),[]),
                                                     (T('Compare Financial Services pricing index of  previous years'), False, URL('default', 'finprice'),[]),
                                                     (T('Compare Transportation Services pricing index of  previous years'), False, URL('default', 'transprice'),[]),
                                                     (T('Compare Food and Beverage pricing index of  previous years'), False, URL('default', 'foodprice'),[]),
                                                     (T('Compare Motor vehicles pricing index of  previous years'), False, URL('default', 'motorprice'),[]),
                                                     (T('More to Come..Watch this space!!!'), False, False,[]),
],),
]
if DEVELOPMENT_MENU: _()

if "auth" in locals(): auth.wikimenu() 
